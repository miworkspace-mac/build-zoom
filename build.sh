#!/bin/bash -ex

# CONFIG
prefix="Zoom"
suffix=""
munki_package_name="Zoom"
display_name="Zoom"
icon_name=""
category="Utilities"
description="Simplified video conferencing and messaging across any device."
url=`./finder.sh`

# download it
curl -L -o app.pkg $url

## Unpack
/usr/sbin/pkgutil --expand app.pkg pkg
mkdir -p build-root/Applications
(cd build-root/Applications; pax -rz -f ../../pkg/*.pkg/Payload)

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | head -1 | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.pkg --postinstall_script postinstall.sh ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

# Obtain version info
version=`defaults read "${plist}" version`

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.12.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" icon_name "${icon_name}"

defaults write "${plist}" unattended_install -bool YES
#defaults write "${plist}" supported_architectures -array x86_64


# Obtain display name from Izzy and add to plist
displayname=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"


# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist

# Notify trello
#ruby $HOME/jenkins-trello/trello-notify.rb "In Testing" "zoom" "${version}" "${prefix}-${version}${suffix}.plist" free

