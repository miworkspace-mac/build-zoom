#!/bin/bash

NEWLOC=`curl -L -I "https://zoom.us/client/latest/ZoomInstallerIT.pkg" -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.0 Safari/605.1.15' 2>/dev/null | grep -i 'location' | tail -1 | sed 's/location: //ig' | tr -d '\r'`

if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi

