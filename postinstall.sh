#!/bin/bash -ex

# copy audio driver into place
mkdir -p /Library/Audio/Plug-Ins/HAL/
cp -R /Applications/zoom.us.app/Contents/PlugIns/ZoomAudioDevice.driver /Library/Audio/Plug-Ins/HAL/ZoomAudioDevice.driver

# restart audio service
killall coreaudiod


# restart audio service a second time to work around a bug
sleep 10
killall coreaudiod

# disable auto-updates
defaults write /Library/Preferences/us.zoom.config.plist ZAutoUpdate 0

defaults write /Library/Preferences/us.zoom.config.plist ZAutoSSOLogin 1

defaults write /Library/Preferences/us.zoom.config.plist ZSSOHost -string umich.zoom.us

defaults write /Library/Preferences/us.zoom.config.plist EnableSilentAutoUpdate 1

exit 0